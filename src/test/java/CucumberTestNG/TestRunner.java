package CucumberTestNG;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
features = "src/test/resources/feature/TestNG.feature",
glue={"CucumberTestNG"})
public class TestRunner extends AbstractTestNGCucumberTests  {

}
